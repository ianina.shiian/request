(function() {
  class HttpRequest {
    // get request options({ baseUrl, headers })
    constructor({ baseUrl, headers }) {
      this.baseUrl = baseUrl;
      this.headers = headers;
    }

    get(url, config = {}) {
      return this.createRequest('GET', url, config);
    }

    post(url, config = {}) {
      return this.createRequest('POST', url, config);
    }

    static setUrl(baseUrl, givenUrl, params) {
      const createdUrl = new URL(givenUrl, baseUrl);

      if (params) {
        Object.entries(params).forEach(e => createdUrl.searchParams.set(...e));
      }

      return createdUrl;
    }

    setHeaders(xhr, optionalHeaders) {
      const requestHeaders = { ...this.headers, ...optionalHeaders };
      Object.entries(requestHeaders).forEach(e => {
        xhr.setRequestHeader(...e);
      });
    }

    createRequest(method, url, config) {
      const xhr = new XMLHttpRequest();

      const {
        transformResponse = [],
        headers,
        params,
        data = null,
        responseType = 'json',
        onUploadProgress,
        onDownloadProgress
      } = config;

      const requestUrl = HttpRequest.setUrl(this.baseUrl, url, params);

      xhr.responseType = responseType;
      this.setHeaders(xhr, headers);
      xhr.open(method, requestUrl);

      if (onUploadProgress) {
        xhr.upload.onprogress = onUploadProgress;
      }

      if (onDownloadProgress) {
        xhr.onprogress = onDownloadProgress;
      }

      xhr.send(data);

      return new Promise((resolve, reject) => {
        xhr.onload = function() {
          if (xhr.status !== 200) {
            // analyze HTTP status of the response
            const error = new Error(`${xhr.status}: ${xhr.statusText}`);
            // e.g. 404: Not Found

            return reject(error);
          }

          if (transformResponse.length > 0) {
            const responseData = transformResponse.reduce(
              (acc, func) => func(acc),
              xhr.response
            );

            return resolve(responseData);
          }
          return resolve(xhr.response);
        };

        xhr.onerror = function() {
          return reject(new Error('Request failed'));
        };
      });
    }
  }

  /*
const reuest = new HttpRequest({
  baseUrl: 'http://localhost:3000',
});

reuest.get('/user/12345', { onDownloadProgress, headers: {contentType: undefined} })
  .then(response => {
    console.log(response);
  })
  .catch(e => {
    console.log(e)
  });

reuest.post('/save', { data: formdata, header, onUploadProgress })
  .then(response => {
    console.log(response);
  })
  .catch(e => {
    console.log(e)
  });

config = {

  // `transformResponse` allows changes to the response data to be made before
  // it is passed to then/catch
  transformRespons  e: [function (data) {
    // Do whatever you want to transform the data

    return data;
  }],

  // `headers` are custom headers to be sent
  headers: {'X-Requested-With': 'XMLHttpRequest'},

  // `params` are the URL parameters to be sent with the request
  // Must be a plain object or a URLSearchParams object
  params: {
    ID: 12345
  },

  // `data` is the data to be sent as the request body
  // Only applicable for request methods 'PUT', 'POST', and 'PATCH'
  // When no `transformRequest` is set, must be of one of the following types:
  // - string, plain object, ArrayBuffer, ArrayBufferView, URLSearchParams
  // - Browser only: FormData, File, Blob
  // - Node only: Stream, Buffer

  data: {
    firstName: 'Fred'
  },

  // `responseType` indicates the type of data that the server will respond with
  // options are 'arraybuffer', 'blob', 'document', 'json', 'text', 'stream'
  responseType: 'json', // default

  // `onUploadProgress` allows handling of progress events for uploads
  onUploadProgress: function (progressEvent) {
    // Do whatever you want with the native progress event
  },

  // `onDownloadProgress` allows handling of progress events for downloads
  onDownloadProgress: function (progressEvent) {
    // Do whatever you want with the native progress event
  },
}
*/
  window.HttpRequest = HttpRequest;
}());
