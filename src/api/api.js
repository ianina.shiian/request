/* global HttpRequest */

(function() {
  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'multipart/form-data');

  const request = new HttpRequest({
    baseUrl: 'http://localhost:8000',
    baseHeaders: myHeaders
  });

  const api = request => ({
    getList: config => request.get('/list', config),
    uploadFile: (url = '/upload', config) => request.post(url, config),
    downloadFile: (url = '/files', config) => request.get(url, config)
  });

  window.api = api(request);
}());
