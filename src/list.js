(function() {
  const cbSelectedItem = null;

  function clearFileList(node) {
    node.innerHTML = '';
  }

  function listHandler(e, cbSelectedItem) {
    const currElement = e.target.innerHTML;
    const indexElement = currElement.indexOf(';');
    const newString = currElement.slice(indexElement + 1);

    cbSelectedItem(newString);
  }

  function renderFileList(files, node) {
    const fragment = document.createDocumentFragment();

    files.forEach((e, index) => {
      let i = index;
      const li = document.createElement('li');
      li.innerHTML = `${(i += 1)}.&nbsp;${e}`;
      fragment.appendChild(li);
    });

    node.appendChild(fragment);
  }

  function addClickEventNode(node, cbSelectedItem) {
    node.addEventListener('click', e => listHandler(e, cbSelectedItem));
  }

  function getFilesList(node, api, cbSelectedItem) {
    api
      .getList('/list', {
        responseType: 'json'
      })
      .then(data => {
        clearFileList(node);
        renderFileList(data, node);
        addClickEventNode(node, cbSelectedItem);
      })
      .catch(error => {
        throw new Error(error);
      });
  }

  window.getFilesList = getFilesList;
}());
