/* global getFilesList, api, setValueDownloadInput, initUploadForm, initDownloadForm */
(function() {
  const list = document.querySelector('.list');

  const uploadForm = document.querySelector('.uploadForm');
  const downloadForm = document.querySelector('.downloadForm');

  getFilesList(list, api, setValueDownloadInput);

  initUploadForm(uploadForm, api, () =>
    getFilesList(list, api, setValueDownloadInput)
  );

  initDownloadForm(downloadForm, api);
}());
