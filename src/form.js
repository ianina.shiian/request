/* global api,  progressBar*/

(function() {
  const inputFileName = document.querySelector('.inputValue');

  function setValueDownloadInput(newString) {
    inputFileName.value = newString;
  }

  function downloadImage(path, inputVal) {
    const fileName = inputVal.split('.')[0];
    const img = document.querySelector('.imgInner');

    img.src = path;
    img.alt = fileName;
  }

  function uploadFile(blob, inputVal) {
    const data = window.URL.createObjectURL(blob);
    const link = document.createElement('a');

    link.href = data;
    link.download = inputVal;
    link.click();

    setTimeout(function() {
      window.URL.revokeObjectURL(data);
    }, 100);
  }

  function onUploadSubmit(e, api, callBack) {
    e.preventDefault();

    const form = new FormData();
    form.append('sampleFile', e.target.sampleFile.files[0]);

    api
      .uploadFile('/upload', {
        data: form,
        onUploadProgress: e => progressBar(e, '.progress_1', '.percentage')
      })
      .then(callBack)
      .catch(error => {
        throw new Error(error);
      });
  }

  function onDownloadSubmit(e, api) {
    e.preventDefault();
    const inputVal = inputFileName.value;

    if (inputVal) {
      api
        .downloadFile(`/files/${inputVal}`, {
          onDownloadProgress: e => progressBar(e, '.progress_2'),
          responseType: 'blob'
        })
        .then(data => {
          if (data.type === 'image/png' || data.type === 'image/jpeg') {
            const path = `/files/${inputVal}`;

            return downloadImage(path, inputVal);
          }
          return uploadFile(data, inputVal);
        })
        .catch(error => {
          throw new Error(error);
        });
    }
  }

  function initUploadForm(node, api, callBack) {
    node.addEventListener('submit', e => onUploadSubmit(e, api, callBack));
  }

  function initDownloadForm(node, api) {
    node.addEventListener('submit', e => onDownloadSubmit(e, api));
  }

  window.inputFileName = inputFileName;
  window.setValueDownloadInput = setValueDownloadInput;
  window.initUploadForm = initUploadForm;
  window.initDownloadForm = initDownloadForm;
}());
