/* global api */
(function() {
  function progressBar(e, input, percent) {
    if (e.total === 150) {
      return;
    }

    const progress = document.querySelector(input);

    if (percent) {
      const percentage = document.querySelector(percent);
      const status = ((e.loaded * 100) / e.total).toFixed(0);
      percentage.innerHTML = `${status}%`;
    }

    progress.value = e.loaded;
    progress.max = e.total;
  }

  window.progressBar = progressBar;
}());
